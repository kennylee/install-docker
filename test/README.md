# 测试和更新说明

## 测试文件

把Vagrantfile文件拷贝到offline或online下，然后执行test-docker-offline.sh或test-docker-online.sh即可进入虚拟环境下测试。

* online

	```
	cd test
	sh sh test-docker-online.sh
	```
	
* offline

	```
	cd test
	sh sh test-docker-offline.sh
	```

* clean

    ```
    # 执行完需要清理下
    sh clean-test-vm.sh
    ```

## 更新方法

1. 修改Vagrantfile文件中具体box的名字和版本。
2. 更新离线文件(update-offline-packages.py)。
3. 执行online脚本测试。
4. 断开网络，执行offline脚本测试。


