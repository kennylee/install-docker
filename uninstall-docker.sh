#! /bin/bash

##### Constants
DOCKER_LIB_HOME=/home/docker/lib/
DOCKER_CONFIG_DIR=/etc/systemd/system/docker.service.d
DOCKER_CONFIG_FILE="${DOCKER_CONFIG_DIR}/docker.conf"

sudo yum -y remove docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-engine \
        docker-ce \
        docker-ce-cli

sudo pip uninstall docker-compose

# confirm to delete
if [[ -d ${DOCKER_LIB_HOME} ]]; then
    sudo rm -r ${DOCKER_LIB_HOME}
fi

if [[ -f ${DOCKER_CONFIG_FILE} ]]; then
    sudo rm ${DOCKER_CONFIG_FILE}
fi
